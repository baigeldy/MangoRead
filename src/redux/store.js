import { configureStore } from "@reduxjs/toolkit";
import mangaSlice from "./slices/mangaSlice";
import messageSlice from "./slices/messageSlice";

export const store = configureStore({
  reducer: {
    manga: mangaSlice,
    message: messageSlice,
  },
});
