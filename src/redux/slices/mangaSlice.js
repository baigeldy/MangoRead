import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { concatenateObjectValues } from "../../selectors";

export const fetchManga = createAsyncThunk("manga/fetchManga", async () => {
  const response = await fetch("https://api.jikan.moe/v4/anime");
  const data = await response.json();
  return data.data;
});

const mangaSlice = createSlice({
  name: "manga",
  initialState: {
    all_manga: [],
    status: "idle",
    error: null,
    filtered_manga: [],
    filtered_manga2: [],
  },
  reducers: {
    filterManga: (state, { payload: filters }) => {
      const filteredManga = state.all_manga.filter((mangaItem) => {
        return Object.keys(filters).every((filterKey) => {
          const filterRegExp = (prop) =>
            filters[prop].replace(/^\s+|\s+$/g, "").replace(/\s+/g, "|");
          if (filterKey === "type") {
            const searchFields = ["source", "type"];
            return searchFields.some((prop) =>
              new RegExp(filterRegExp("type"), "g").test(mangaItem[prop])
            );
          } else {
            return new RegExp(filterRegExp("other"), "i").test(
              concatenateObjectValues(mangaItem)
            );
          }
        });
      });
      return {
        ...state,
        filtered_manga: filteredManga.filter((mi) => {
          const year = mi.aired?.prop?.from?.year;
          return (
            year >= filters.year.from &&
            (filters.year.to === null || year <= filters.year.to)
          );
        }),
        filtered_manga2:
          filters.type.trim().length && filters.other === ""
            ? filteredManga
            : state.filtered_manga2,
      };
    },
    resetFilteredArrs: (state) => {
      return { ...state, filtered_manga: [], filtered_manga2: [] };
    },
    searchManga: (state, { payload }) => {
      return {
        ...state,
        filtered_manga: state.all_manga.filter((i) =>
          new RegExp(payload, "gi").test(concatenateObjectValues(i))
        ),
      };
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchManga.pending, (state) => {
        state.status = "loading";
        state.error = null;
      })
      .addCase(fetchManga.fulfilled, (state, { payload }) => {
        state.all_manga = payload;
        state.status = "resolved";
        state.error = null;
      })
      .addCase(fetchManga.rejected, (state) => {
        state.status = "rejected";
        state.error = null;
      });
  },
});

export const { actions, reducer: mangaReducer } = mangaSlice;

export default mangaSlice.reducer;
