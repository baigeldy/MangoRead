import { createSlice } from "@reduxjs/toolkit";

const messageSlice = createSlice({
  name: "message",
  initialState: {
    text: "",
    type: "",
    isShown: false,
    isImportant: false,
  },
  reducers: {
    setMessage(state, { payload }) {
      state.text = payload;
      state.isShown = true;
    },
  },
});

export const { actions, reducer } = messageSlice;

export default messageSlice.reducer;
