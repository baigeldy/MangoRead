import { useDispatch } from "react-redux";
import Filters from "./components/FiltersPanel";
import { actions } from "./redux/slices/mangaSlice";
import Header from "./components/Header";
import List from "./components/List";

const App = () => {
  const dispatch = useDispatch();
  const handleSearch = (e) => {
    e.preventDefault();
    dispatch(actions.searchManga(e.target[0].value));
  };
  return (
    <>
      <Header handleSearch={handleSearch} />
      <main className="manga__row font-montserrat flex flex-col container mx-auto py-10 md:flex-row justify-center">
        <Filters />
        <List />
      </main>
    </>
  );
};

export default App;
