import { bindActionCreators } from "@reduxjs/toolkit";
import { actions } from "../redux/slices/mangaSlice";
import { useDispatch } from "react-redux";
import { useMemo } from "react";

const rootActions = {
  ...actions,
};

export const useActions = () => {
  const dispatch = useDispatch();
  return useMemo(() => bindActionCreators(rootActions, dispatch), [dispatch]);
};
