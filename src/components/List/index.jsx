import { fetchManga } from "../../redux/slices/mangaSlice";
import { mangaSelector, selectFilteredAnime } from "../../selectors";
import { useDispatch, useSelector } from "react-redux";
import AnimeCard from "../AnimeCard";
import { useEffect, useState } from "react";

const List = () => {
  const dispatch = useDispatch();
  const all_manga = useSelector(mangaSelector);
  const filteredManga = useSelector(selectFilteredAnime);
  useEffect(() => {
    dispatch(fetchManga());
  }, [dispatch]);
  return (
    <div className="flex flex-wrap gap-4 justify-center w-full">
      {{
        arr: filteredManga.length ? filteredManga : all_manga,
      }.arr.map((item, idx) => (
        <AnimeCard anime={item} key={idx} />
      ))}
    </div>
  );
};

export default List;
