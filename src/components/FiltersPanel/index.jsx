import { actions } from "../../redux/slices/mangaSlice";
import { useDispatch, useSelector } from "react-redux";
import { selectAllAnimeTypes } from "../../selectors";
import React, { useState } from "react";
import YearFilter from "./YearFilter";

const FiltersPanel = () => {
  const dispatch = useDispatch();
  const { filterManga, resetFilteredArrs } = actions;
  const initialFilters = {
    type: "",
    other: "",
    active: false,
    year: { from: 1900, to: 2023 },
  };
  const [filters, setLocalFilters] = useState(initialFilters);
  const all_manga_types = useSelector(selectAllAnimeTypes(filters));
  const resetFilters = () => {
    setLocalFilters(initialFilters);
    dispatch(resetFilteredArrs());
  };

  const addFilterField = (e) => {
    if (filters.active === false) {
      if (filters.type.includes(e.target.innerText)) {
        setLocalFilters((prevFilters) => ({
          ...prevFilters,
          type: filters.type
            .replace(e.target.innerText, "")
            .replace(/ +/g, " "),
        }));
      } else {
        setLocalFilters((prevFilters) => ({
          ...prevFilters,
          type: prevFilters.type + " " + e.target.innerText,
        }));
      }
    } else {
      if (filters.other.includes(e.target.innerText)) {
        setLocalFilters((prevFilters) => ({
          ...prevFilters,
          other: filters.other
            .replace(e.target.innerText, "")
            .replace(/ +/g, " "),
        }));
      } else {
        setLocalFilters((prevFilters) => ({
          ...prevFilters,
          other: prevFilters.other + " " + e.target.innerText,
        }));
      }
    }
  };
  return (
    <div className="filters w-max p-3 h-[43rem] rounded-lg bg-white shadow-md mx-auto mb-10 md:mx-0 md:my-0">
      <div
        style={
          all_manga_types.length === 5 && all_manga_types[4] !== "Mystery"
            ? { display: "none" }
            : { display: "flex" }
        }
        className="filters__head flex w-64 h-11 items-center gap-2 flex-shrink-0 py-[0.3rem]"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="15"
          height="25"
          viewBox="0 0 19 25"
          fill="none"
        >
          <path
            d="M15 2L3 14L15 26"
            stroke="#878787"
            strokeWidth="3"
            strokeLinecap="round"
          />
        </svg>
        <span
          className="text-gray-500 cursor-pointer active:translate-x-1 font-normal text-xl"
          onClick={resetFilters}
        >
          Назад
        </span>
      </div>
      <div
        style={
          all_manga_types.length === 5 && all_manga_types[4] !== "Mystery"
            ? { display: "flex" }
            : { display: "none" }
        }
        className="filters__head flex w-64 h-11 items-center gap-2 flex-shrink-0 py-[0.3rem] justify-between"
      >
        <span
          className="text-black font-montserrat text-[1.4rem]"
          onClick={() => setLocalFilters(initialFilters)}
        >
          Жанры
        </span>
        <div className="flex items-center gap-1">
          <span
            className="translate-y-[0.1rem] text-xl text-gray-400 font-medium"
            onClick={() => {
              const updatedFilters = {
                ...filters,
                type: all_manga_types.join(" "),
                active: true,
              };
              setLocalFilters(updatedFilters);
              dispatch(filterManga(updatedFilters));
            }}
          >
            все
          </span>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="15"
            height="25"
            viewBox="0 0 19 25"
            fill="none"
            className="rotate-180 translate-y-1"
          >
            <path
              d="M15 2L3 14L15 26"
              stroke="#878787"
              strokeWidth="3"
              strokeLinecap="round"
            />
          </svg>
        </div>
      </div>
      <br />
      <div className="filters__genres inline-flex h-113 flex-col items-start gap-2 flex-shrink-0 w-full">
        <h2 className="filters__genres-title text-black text-3xl font-normal leading-normal">
          {all_manga_types.length === 5 && all_manga_types[4] !== "Mystery"
            ? "Тип"
            : "Жанры"}
        </h2>
        <div className="hdscrlbr max-h-[35rem] overflow-y-auto w-full">
          <ul className="flex flex-col gap-2 h-[30rem] w-full">
            {all_manga_types.map((i) => (
              <li
                key={i}
                className="flex text-[1.5rem] gap-1 items-center cursor-pointer active:text-gray-400"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="30"
                  height="30"
                  viewBox="0 0 30 30"
                  fill="none"
                  className={`cursor-pointer ${
                    new RegExp(i, "g").test(filters["type"]) ||
                    new RegExp(i, "gi").test(filters["other"]) === true
                      ? "fill-lime-600"
                      : ""
                  }`}
                >
                  <rect
                    x="1"
                    y="1"
                    width="28"
                    height="28"
                    rx="4"
                    stroke="#2FE09B"
                    strokeWidth="2"
                  />
                </svg>{" "}
                <span onClick={addFilterField}>{i}</span>
              </li>
            ))}
            <YearFilter filters={filters} setLocalFilters={setLocalFilters} />
          </ul>
        </div>
      </div>
      <div className="flex items-center justify-between">
        <button
          className="flex px-4 py-3 w-[48%] active:translate-y-1 justify-center items-center gap-2 rounded-md bg-purple-600 text-white my-2"
          onClick={resetFilters}
        >
          Сбросить
        </button>
        <button
          className="flex px-4 py-3 w-[48%] active:translate-y-1 justify-center items-center gap-2 rounded-md bg-purple-600 text-white my-2"
          disabled={
            (filters.type.trim() === "" &&
              filters.year.from === 1900 &&
              filters.year.to === 2023) ||
            (filters.type.trim() !== "" &&
              filters.other === "" &&
              filters.year.from === 1900 &&
              filters.year.to === 2023 &&
              filters.active === true)
              ? true
              : false
          }
          onClick={() => {
            if (
              filters.year.from !== 1900 &&
              filters.year.to !== 2023 &&
              filters.type.trim().length > 0
            ) {
              setLocalFilters({ ...filters, active: true });
              dispatch(filterManga(filters));
            } else if (
              filters.year.from !== 1900 &&
              filters.year.to !== 2023 &&
              filters.type.trim().length == 0
            ) {
              setLocalFilters({ ...filters, active: false });
              dispatch(filterManga(filters));
            } else {
              setLocalFilters({ ...filters, active: true });
              dispatch(filterManga(filters));
            }
          }}
        >
          Применить
        </button>
      </div>
    </div>
  );
};

export default FiltersPanel;
