import React from "react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { selectFilteredAnime } from "../../selectors";

const YearFilter = ({ filters, setLocalFilters }) => {
  const fm = useSelector(selectFilteredAnime);
  useEffect(() => {
    if (filters.year.from !== 1900 || filters.year.to !== 2023) {
      setLocalFilters({
        ...filters,
        active: fm.length && filters.type.trim().length ? true : false,
      });
    }
  }, [filters.year.from, filters.year.to]);

  return (
    <div className="flex space-x-2 justify-center">
      <input
        type="number"
        placeholder="From 0"
        className="border-green-500 border-2 rounded h-10 px-[1rem] w-[7.8rem]"
        onChange={(e) =>
          setLocalFilters({
            ...filters,
            year: { ...filters.year, from: +e.target.value },
          })
        }
        onMouseEnter={(e) => e.target.setAttribute("value", filters.year.from)}
        onMouseLeave={(e) =>
          e.target.removeAttribute("value", filters.year.from)
        }
        min="1900"
        max="2020"
      />
      <span className="self-center font-bold">—</span>
      <input
        type="number"
        placeholder="To 2022"
        className="border-green-500 border-2 rounded h-10 px-[1rem] w-[7.8rem]"
        onChange={(e) =>
          +e.target.value
            ? setLocalFilters({
                ...filters,
                year: { ...filters.year, to: +e.target.value },
              })
            : ""
        }
        onMouseEnter={(e) => e.target.setAttribute("value", filters.year.to)}
        onMouseLeave={(e) => e.target.removeAttribute("value", filters.year.to)}
        min="1900"
        max="2020"
      />
    </div>
  );
};

export default YearFilter;
