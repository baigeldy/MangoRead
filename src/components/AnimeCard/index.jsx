import React from "react";

const AnimeCard = ({ anime }) => {
  const { images, titles, aired } = anime;
  const imageUrl = images.jpg.image_url;
  const year = aired.prop.from.year;
  const title = titles.find((t) => t.type === "Default").title;
  
  return (
    <div className="relative w-[13.5rem] h-[15.0em] rounded-lg overflow-hidden shadow-lg cursor-pointer">
      <div
        className="absolute inset-0 bg-cover bg-center hover:bg-gray-800 transition duration-300"
        style={{ backgroundImage: `url(${imageUrl})` }}
      >
        <div className="absolute inset-0 bg-black opacity-0 hover:opacity-75 transition duration-300"></div>
      </div>
      <div className="absolute bottom-0 left-0 p-4 text-white">
        <p className="text-lg">{year}</p>
        <p className="text-xl font-bold">{title}</p>
      </div>
    </div>
  );
};

export default AnimeCard;
