import search_icon from "../../assets/search_icon.svg";
import logo from "../../assets/Logo (3).svg";

const Header = ({handleSearch}) => {
  return (
    <header className="header w-full h-[6.875rem] justify-center bg-gray-100 shadow-md flex place-content-center items-center">
      <div className="container">
        <div className="header__inner mx-auto flex items-center justify-between max-w-[77.5rem]">
          <div className="header__logo flex items-center">
            <img
              src={logo}
              alt="manga-read-logo"
              className="w-[6.4375rem] h-[4.3125rem]"
            />
            <div className="flex flex-col">
              <h3 className="text-black text-lg font-medium">MangoRead</h3>
              <p className="text-gray-500 text-base">Читай мангу с нами</p>
            </div>
          </div>
          <label className="header__search px-1 py-[0.88rem] rounded-md border-2 border-gray-500 bg-white flex items-center gap-4 flex-shrink-0 max-w-72 h-14 p-2.5 hidden sm:flex">
            <img src={search_icon} alt="" className="max-w-7 scale-100 h-7" />
            <form onSubmit={handleSearch}>
              <input
                type="text"
                aria-label="idk"
                placeholder="Placeholder"
                className="bg-transparent font-normal text-xl outline-none border-none"
              />
            </form>
          </label>
          <button className="flex items-center justify-center p-2 border border-gray-300 rounded-md hover:bg-gray-100 focus:outline-none sm:hidden">
            <span className="sr-only">Toggle Menu</span>
            <svg
              className="w-10 h-10 fill-current"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M2 6h16M2 10h16M2 14h16"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </button>
          <div className="header__buttons flex gap-2 lg:block hidden">
            <button className="header__btn inline-flex p-3.75 pt-[0.8rem] pb-[0.8rem] pl-10 pr-10 justify-center items-center gap-2.5 rounded-md border-2 border-purple-700">
              Войти
            </button>
            <button className="header__btn ml-[0.94rem] inline-flex p-3.75 pt-[0.8rem] pb-[0.8rem] rounded-md bg-purple-700 text-white pl-10 pr-10 justify-center items-center gap-2.5">
              Регистрация
            </button>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
