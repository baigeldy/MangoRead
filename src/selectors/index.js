export const mangaSelector = (state) => state.manga?.all_manga;

export const selectAllAnimeTypes = (filters) => (state) => {
  let types = [
    ...new Set(state.manga.all_manga?.map((i) => i.source)),
    ...new Set(state.manga.all_manga?.map((j) => j.type)),
  ];
  let genres = [
    ...new Set(
      state.manga.filtered_manga2?.map((i) => {
        i.genres.forEach((j) => (i = j.name));
        return i;
      })
    ),
  ];
  if (filters.type.trim() === "" || filters.active === false) {
    return types;
  } else {
    return genres;
  }
};

export const selectFilteredAnime = (state) => state?.manga?.filtered_manga;
export const concatenateObjectValues = (obj) => {
  let result = "";
  function traverse(obj) {
    for (const key in obj) {
      const value = obj[key];

      if (typeof value === "object" && value !== null) {
        traverse(value);
      } else {
        result += String(value) + " ";
      }
    }
  }
  traverse(obj);
  return result.trim();
};
